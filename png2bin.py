import os
import sys
from PIL import Image

pngfile = Image.open(sys.argv[1])
assert pngfile.mode == 'P'
binfile = open(os.path.splitext(sys.argv[1])[0]+'.bin','wb')

for y in range(0,200):
    for x in range(0,320):
        binfile.write(pngfile.getpixel((x,y)).to_bytes(1,byteorder='little'))

binfile.close()

pal = pngfile.getpalette()
palbytes = bytearray()
for c in pal:
    palbytes.append(c>>2)

palfile = open(os.path.splitext(sys.argv[1])[0]+'.pal','wb')
palfile.write(palbytes)
palfile.close()

pngfile.close()